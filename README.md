README

* you should have maven, git and jdk preinstalled locally
* clone project locally
* execute from root folder in terminal : mvn clean spring-boot:run
* goto : localhost:8080/console
* change there the connection database according to ${spring.datasource.url} value in resources/application.properties
* checkout created tables
* that is it for now. Enjoy!
