package test.converterboot.service.receiver;

import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.service.receiver.JsonDataReceiver;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import test.converterboot.TestContextConfig;

/**
 * Created by Alexander on 23.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfig.class)
public class JsonDataReceiverTest {

    @Autowired
    private JsonDataReceiver dataReciever;

    @Before
    public void setup() {
    }

    @After
    public void cleanUp() {
    }

    @Test
    public void accertNotNull() {
//      when
        List<Rate> dataList = dataReciever.receiveRates();
//      then
        assertThat(dataList, is(not(nullValue())));
    }

    @Test
    public void accertCountIsCorrect() {
        final int correctCcyCount = CurrencyPairEnum.values().length;
        //when
        List<Rate> dataList = dataReciever.receiveRates();
        //then
        assertThat(dataList.size(), is(equalTo(correctCcyCount)));
    }

}
