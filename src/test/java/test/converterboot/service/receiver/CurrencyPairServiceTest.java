package test.converterboot.service.receiver;

import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import gq.kiev.converterboot.service.db.CurrencyPairService;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import test.converterboot.TestContextConfig;

/**
 * Created by Alexander on 23.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestContextConfig.class)
public class CurrencyPairServiceTest {
    
    
    @Autowired
    private DatabaseInitialisationService dbInitService;

    @Autowired
    private CurrencyPairService currencyPairService;

    @Before
    public void setup() {
        dbInitService.initCurrencyPair();
        dbInitService.initScale();
    }

    @After
    public void cleanUp() {
    }

    @Test
    public void assertNull() {
        CurrencyPair pair = currencyPairService.getCurrencyPair("BTC");
        assertThat(pair, is(nullValue()));
    }
    
    @Test
    public void testMandatoryPairNamesNotNull() {
        List<String> mandatoryPairNames = new ArrayList<>();
        for(CurrencyPairEnum currPairEnum : CurrencyPairEnum.values()){
            mandatoryPairNames.add(currPairEnum.getName());
        }

        for(String pairName : mandatoryPairNames){
            CurrencyPair pair = currencyPairService.getCurrencyPair(pairName);
            assertThat(pair, is(notNullValue()));
        }
    }

}
