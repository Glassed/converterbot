package test.converterboot;

import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by illia on 14.03.16.
 */
@EnableAutoConfiguration
@ComponentScan({"gq.kiev.converterboot.service.receiver"
        , "gq.kiev.converterboot.service.db"})
@Configuration
//ATENTION
@EnableJpaRepositories("gq.kiev.converterboot.dao")
@EntityScan(basePackages = "gq.kiev.converterboot.model.db")
public class TestContextConfig {

    @Test
    public void runSpringDataTest() {

    }

}