package test.converterboot.dbinit;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by Alexander on 23.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DBIniTestContextConfig.class)
public class DBInitServiceCurrPareTest {
    
    //Wiring dao's:
    @Autowired
    private CurrencyPairDao pairDao;    
    @Autowired
    private ScaleDao scaleDao;
    
    
    
    @Autowired
    DatabaseInitialisationService initService;

    private void initDB() {
        initService.initCurrencyPair();
        initService.initScale();
    }
    
    @Test
    public void testScalesContaining() {
        //init db:
        initDB();
        
        //prerare variables:
        List<String> container  = new ArrayList<>();        
        for (Scale scale : scaleDao.findAll()) {
            container.add(scale.getScale());
        }
        
        List<String> mustHave = new ArrayList<>();
        for(ScaleEnum scaleEnumItem : ScaleEnum.values()){
            mustHave.add(scaleEnumItem.getName());
        }
        
        //then        
        assertTrue(isContainAll(container, mustHave));        
    }
    
    @Test
    public void testPairsContaining() {
        //init db:
        initDB();
        
        //prerare variables:
        List<String> container  = new ArrayList<>();        
        for (CurrencyPair pair : pairDao.findAll()) {
            container.add(pair.getPair());
        }
        
        List<String> mustHave = new ArrayList<>();
        for(CurrencyPairEnum currencyPairEnum : CurrencyPairEnum.values()){
            mustHave.add(currencyPairEnum.getName());
        }
                
        //then        
        assertTrue(isContainAll(container, mustHave));        
    }
    
    @Test
    public void testScaleDbValuesNotIncr(){
        //init db:
        initDB();
        
        int scaleCountBefore = 0;
        int scaleCountAfter = 0;
        for(Scale scale : scaleDao.findAll()){
            scaleCountBefore++;
        }
        testScalesContaining();
        for(Scale scale : scaleDao.findAll()){
            scaleCountAfter++;
        }
        
        assertEquals(scaleCountBefore, scaleCountAfter);
        
    }
    
    
    @Test
    public void testPairDbValuesNotIncr(){
        //init db:
        initDB();
        
        int currPairCountBefore = 0;
        int currPairCountAfter = 0;
        for(CurrencyPair pair : pairDao.findAll()){
            currPairCountBefore++;
        }
        testScalesContaining();
        for(CurrencyPair pair : pairDao.findAll()){
            currPairCountAfter++;
        }
        
        assertEquals(currPairCountBefore, currPairCountAfter);
        
    }
    
    /**
     * compares two String arrays. Return false if container do not contain 
     * all mustContain
     * @param container
     * @param mustContain
     * @return 
     */
    private boolean isContainAll(List<String> container
            , List<String> mustContain ){
        
        if(container==null
                ||mustContain==null
                ||container.size()<mustContain.size()){
            return false;
        }
        
        for (String mustBeStoredString : mustContain) {
            boolean stringNotContained = true;
            for (String containedString : container) {
                if (containedString.equalsIgnoreCase(mustBeStoredString)) {
                    stringNotContained = false;
                }
            }
            if(stringNotContained){
                return false;
            }
        }
        return true;
    }

}
