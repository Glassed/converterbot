package test.converterboot.dbinit;

import test.converterboot.dao.*;
import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Alexander on 23.03.16.
 */
@EnableAutoConfiguration
@ComponentScan({"gq.kiev.converterboot.service.json.receiver"
        , "gq.kiev.converterboot.service.db"})
@Configuration
//ATENTION
@EnableJpaRepositories("gq.kiev.converterboot.dao")
@EntityScan(basePackages = "gq.kiev.converterboot.model.db")
public class DBIniTestContextConfig {

    @Test
    public void runSpringDataTest() {

    }

}