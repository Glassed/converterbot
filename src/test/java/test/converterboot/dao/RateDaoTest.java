package test.converterboot.dao;

import gq.kiev.converterboot.BaseTest;
import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.dao.RateDao;
import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.api.Pair;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import gq.kiev.converterboot.service.db.CurrencyPairService;
import gq.kiev.converterboot.service.receiver.JsonDataReceiver;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gq.kiev.converterboot.service.db.ScaleService;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.*;
import org.joda.time.LocalDate;
import static org.junit.Assert.*;

/**
 * Created by Alexander on 23.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DaoTestContextConfig.class)
public class RateDaoTest {

    @Autowired
    private RateDao rateDao;

    @Autowired
    private CurrencyPairDao pairDao;

    @Autowired
    private ScaleDao scaleDao;

    @Autowired
    private ScaleService scaleService;

    @Autowired
    private CurrencyPairService currencyPairService;

    @Autowired
    private JsonDataReceiver receiver;

    @Autowired
    private DatabaseInitialisationService initService;

    private Scale defaultDayScale;
    private CurrencyPair defaultCP;

    @Before
    public void initDB() {
        cleanup();
        initService.initCurrencyPair();
        initService.initScale();

        defaultDayScale = scaleService.getByName(ScaleEnum.DAY.getName());
        defaultCP = currencyPairService.getCurrencyPair(CurrencyPairEnum.USDUAH.getName());
    }

    @After
    public void cleanup() {
        rateDao.deleteAll();
        scaleDao.deleteAll();
        pairDao.deleteAll();
    }

    @Test
    public void testPlaceHolder() {
        //when
        Rate rate = receiver.receiveRate(CurrencyPairEnum.EURUAH);
        rate = rateDao.save(rate);

        //then
        assertThat(rate.getId(), is(not(nullValue())));
    }

    @Test
    public void testCustomPair() {
        //when
        //preparing custom currency pair
        int uanUahId = 4;
        String uanUahPair = "UANUAH";
        CurrencyPair customPurrencyPair = new CurrencyPair(uanUahId, uanUahPair);
        pairDao.save(customPurrencyPair);
        //preparing main data in db:        
        //recieve Rate from internet server:
        Rate rate = receiver.receiveRate(CurrencyPairEnum.EURUAH);

        //set custom pair:
        rate.setCurrencypair(customPurrencyPair);

        //save Rate entity:
        rate = rateDao.save(rate);

        //then
        assertThat(rate.getId(), is(not(nullValue())));
    }

    @Test
    public void testFindRateByIds() {
        //when
        List<Rate> rateList = receiver.receiveRates();

        for (Rate rate : rateList) {
            rateDao.save(rate);
        }

        final Rate rate = rateDao.findByCreatedDateAndCurrencypairIdAndScaleId(
                LocalDate.now()
                , CurrencyPairEnum.EURUAH.getId()
                , ScaleEnum.DAY.getId());

        System.out.println(rate);
        //then
        assertThat(rate.getId(), is(not(nullValue())));
    }

    @Test
    public void getRateAccordingToPeriodSimpleTest() {
        //given
        final Set<Rate> expectedRates = new HashSet<>();
        final LocalDate from = LocalDate.now().minusDays(1);
        final LocalDate to = LocalDate.now().plusDays(2);

        Rate expectedRate1 = new Rate(from, 1, 2, defaultCP, defaultDayScale);
        Rate expectedRate2 = new Rate(LocalDate.now(), 3, 4, defaultCP, defaultDayScale);
        Rate expectedRate3 = new Rate(to, 5, 6, defaultCP, defaultDayScale);
        final Rate unExpectedRate1 = new Rate(from.minusDays(1), 7, 8, defaultCP, defaultDayScale);
        final Rate unExpectedRate2 = new Rate(to.plusDays(1), 9, -158, defaultCP, defaultDayScale);

        expectedRate1 = rateDao.save(expectedRate1);
        expectedRate2 = rateDao.save(expectedRate2);
        expectedRate3 = rateDao.save(expectedRate3);
        rateDao.save(unExpectedRate1);
        rateDao.save(unExpectedRate2);

        expectedRates.add(expectedRate1);
        expectedRates.add(expectedRate2);
        expectedRates.add(expectedRate3);

        //when
        final List<Rate> rates =
                rateDao.getRateAccordingToPeriod(defaultCP.getId(),
                        defaultDayScale.getId(), from, to);

        //then
        assertThat(expectedRates, equalTo(new HashSet<>(rates)));
    }
    
    @Test
    public void rateBuyAndSaleAvgTest() {
        //given
        final Set<Rate> expectedRates = new HashSet<>();
        final LocalDate from = LocalDate.now().minusDays(1);
        final LocalDate to = LocalDate.now().plusDays(2);
        
        
        //setting up buy and sale vals for Rates:
        final double[] buyVals = {15,20,25,30,35};
        final double[] saleVals = {150,200,250,300,350};
        
        final double exptdBuyVal = 20;
        final double exptdSaleVal = 200;
        
        Rate expectedRate1 = new Rate(from, buyVals[0], saleVals[0]
                , defaultCP, defaultDayScale);
        Rate expectedRate2 = new Rate(LocalDate.now(), buyVals[1]
                , saleVals[1], defaultCP, defaultDayScale);
        Rate expectedRate3 = new Rate(to, buyVals[2], saleVals[2]
                , defaultCP, defaultDayScale);
        final Rate unExpectedRate1 = new Rate(from.minusDays(1), buyVals[3]
                , saleVals[3], defaultCP, defaultDayScale);
        final Rate unExpectedRate2 = new Rate(to.plusDays(1), buyVals[4]
                , saleVals[4], defaultCP, defaultDayScale);

        expectedRate1 = rateDao.save(expectedRate1);
        expectedRate2 = rateDao.save(expectedRate2);
        expectedRate3 = rateDao.save(expectedRate3);
        rateDao.save(unExpectedRate1);
        rateDao.save(unExpectedRate2);

        expectedRates.add(expectedRate1);
        expectedRates.add(expectedRate2);
        expectedRates.add(expectedRate3);

        //when
        Double recievedBuyVal = rateDao.getAvgBuy(defaultCP.getId(),
                        defaultDayScale.getId(), from, to);
        
        Double recievedSaleVal = rateDao.getAvgSale(defaultCP.getId(),
                        defaultDayScale.getId(), from, to);

        //then
        assertThat(recievedBuyVal, equalTo(exptdBuyVal));
        assertThat(recievedSaleVal, equalTo(exptdSaleVal));
    }

    @Test
    public void averageRatePairRetrievesSuccessfully() {
        //given
        final Set<Rate> expectedRates = new HashSet<>();
        final LocalDate from = LocalDate.now().minusDays(1);
        final LocalDate to = LocalDate.now().plusDays(2);


        //setting up buy and sale vals for Rates:
        final double[] buyVals = {15,20,25,30,35};
        final double[] saleVals = {150,200,250,300,350};

        final double exptdBuyVal = 20;
        final double exptdSaleVal = 200;

        Rate expectedRate1 = new Rate(from, buyVals[0], saleVals[0]
                , defaultCP, defaultDayScale);
        Rate expectedRate2 = new Rate(LocalDate.now(), buyVals[1]
                , saleVals[1], defaultCP, defaultDayScale);
        Rate expectedRate3 = new Rate(to, buyVals[2], saleVals[2]
                , defaultCP, defaultDayScale);
        final Rate unExpectedRate1 = new Rate(from.minusDays(1), buyVals[3]
                , saleVals[3], defaultCP, defaultDayScale);
        final Rate unExpectedRate2 = new Rate(to.plusDays(1), buyVals[4]
                , saleVals[4], defaultCP, defaultDayScale);

        expectedRate1 = rateDao.save(expectedRate1);
        expectedRate2 = rateDao.save(expectedRate2);
        expectedRate3 = rateDao.save(expectedRate3);
        rateDao.save(unExpectedRate1);
        rateDao.save(unExpectedRate2);

        expectedRates.add(expectedRate1);
        expectedRates.add(expectedRate2);
        expectedRates.add(expectedRate3);

        //when
        final Pair avg = rateDao.getAvgPair(defaultCP.getId(),
                defaultDayScale.getId(), from, to);

        //then
        assertThat(avg, equalTo(new Pair(exptdBuyVal, exptdSaleVal)));
    }

    @Test
    public void rateWillBeCreatedWithTheSameChangedDateAsCreatedDate() {
        //given
        LocalDate createdDate = LocalDate.now();
        Rate rate = new Rate(createdDate, 123, 331, defaultCP, defaultDayScale);
        rate = rateDao.save(rate);

        //when
        final Rate retrievedRate = rateDao.findOne(rate.getId());

        //then
        assertThat(retrievedRate, is(notNullValue()));
        assertThat(retrievedRate.getChangedDate(), equalTo(createdDate));
    }

}
