package test.converterboot.dao;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


/**
 * Created by Alexander on 23.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DaoTestContextConfig.class)
public class CurrencyPairDaoTest {

    @Autowired
    private CurrencyPairDao currencyPairDao;

    
    @Test
    public void testPlaceHolder() {
        int uanUahId = 4;
        String uanUahPair = "UANUAH";
        
        CurrencyPair currencyPair = new CurrencyPair(uanUahId, uanUahPair);
        
        
        currencyPairDao.save(currencyPair);
        
        CurrencyPair loadedCurrencyPair = currencyPairDao
                .findOne(uanUahId);
        System.out.println(loadedCurrencyPair);
                
        //then
        assertThat(loadedCurrencyPair, is(not(nullValue())));
    }
}