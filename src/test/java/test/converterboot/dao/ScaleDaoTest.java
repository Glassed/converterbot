package test.converterboot.dao;

import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.Scale;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


/**
 * Created by Alexander on 23.03.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DaoTestContextConfig.class)
public class ScaleDaoTest {

    @Autowired
    private ScaleDao scaleDao;

    
    @Test
    public void testPlaceHolder() {
        int testSclaeId = 700;
        
        Scale scale = new Scale(testSclaeId, "NEWSCALE");
        
        
        scaleDao.save(scale);
        
        Scale loadedScale = scaleDao.findOne(testSclaeId);
        System.out.println(loadedScale);
                
        //then
        assertThat(loadedScale, is(not(nullValue())));
    }
}