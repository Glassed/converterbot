package gq.kiev.converterboot.service.db.impl;

import gq.kiev.converterboot.BaseTest;
import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import org.joda.time.LocalDate;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class RateServiceImplTest extends BaseTest{

    @Test
    public void gettingRatesByScalePairAndDatesPassedSuccessfully() {
        //given
        final List<Rate> expectedRates = new ArrayList<>();
        final LocalDate from = LocalDate.now().minusDays(1);
        final LocalDate to = LocalDate.now().plusDays(2);

        Rate expectedRate1 = new Rate(from, 1, 2, defaultCP, defaultDayScale);
        Rate expectedRate2 = new Rate(LocalDate.now(), 3, 4, defaultCP, defaultDayScale);
        Rate expectedRate3 = new Rate(to, 5, 6, defaultCP, defaultDayScale);
        final Rate unExpectedRate1 = new Rate(from.minusDays(1), 7, 8, defaultCP, defaultDayScale);
        final Rate unExpectedRate2 = new Rate(to.plusDays(1), 9, -158, defaultCP, defaultDayScale);

        expectedRate1 = rateDao.save(expectedRate1);
        expectedRate2 = rateDao.save(expectedRate2);
        expectedRate3 = rateDao.save(expectedRate3);
        rateDao.save(unExpectedRate1);
        rateDao.save(unExpectedRate2);

        expectedRates.add(expectedRate1);
        expectedRates.add(expectedRate2);
        expectedRates.add(expectedRate3);

        //when
        final List<Rate> rates =
                rateService.getRatesByScalePairAndDates("DAY","USDUAH", from, to);

        //then
        assertThat("\n\rExpected : " + expectedRates + ", \n\r But was : " + rates,
                rates.size(), equalTo(expectedRates.size()));
        assertThat(rates, equalTo(expectedRates));
    }

}