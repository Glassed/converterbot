package gq.kiev.converterboot;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.dao.RateDao;
import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.CurrencyPairService;
import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import gq.kiev.converterboot.service.db.RateService;
import gq.kiev.converterboot.service.db.ScaleService;
import gq.kiev.converterboot.service.receiver.JsonDataReceiver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfig.class)
public class BaseTest {
    @Autowired
    protected RateDao rateDao;

    @Autowired
    protected CurrencyPairDao pairDao;

    @Autowired
    protected ScaleDao scaleDao;

    @Autowired
    protected ScaleService scaleService;

    @Autowired
    protected CurrencyPairService currencyPairService;
    
    @Autowired
    protected RateService rateService;

    @Autowired
    protected JsonDataReceiver receiver;

    @Autowired
    protected DatabaseInitialisationService initService;

    protected Scale defaultDayScale;
    protected CurrencyPair defaultCP;

    @Before
    public void initDB() {
        cleanup();
        initService.initCurrencyPair();
        initService.initScale();

        defaultDayScale = scaleService.getByName(ScaleEnum.DAY.getName());
        defaultCP = currencyPairService.getCurrencyPair(CurrencyPairEnum.USDUAH.getName());
    }

    @After
    public void cleanup() {
        rateDao.deleteAll();
        scaleDao.deleteAll();
        pairDao.deleteAll();
    }

    @Test
    public void emptyTest() {

    }

}
