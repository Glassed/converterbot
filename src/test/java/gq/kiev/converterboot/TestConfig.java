package gq.kiev.converterboot;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.dao.RateDao;
import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.CurrencyPairService;
import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import gq.kiev.converterboot.service.db.ScaleService;
import gq.kiev.converterboot.service.receiver.JsonDataReceiver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by illia on 26.03.16.
 */
@EnableAutoConfiguration
@ComponentScan("gq.kiev.converterboot.service")
@Configuration
@EnableJpaRepositories("gq.kiev.converterboot.dao")
@EntityScan(basePackages = "gq.kiev.converterboot.model.db")
public class TestConfig {


}
