/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.scheduled;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.dao.RateDao;
import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.service.receiver.JsonDataReceiver;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Alexander
 */
@Component
public class ScheduledTaskExecutor {

    @Autowired
    JsonDataReceiver receiver;

    @Autowired
    RateDao rateDao;
    @Autowired
    ScaleDao scaleDao;
    @Autowired
    CurrencyPairDao currencyPairDao;

    //todo set up scheduled task as we need
    @Scheduled(cron = "*/10 * * * * *")
    public void doSomething() {
        List<Rate> rateList = receiver.receiveRates();
        for (Rate rate : rateList) {
            savingMessage(rate);
            rate = rateDao.save(rate);

            if (rate.getId() != null) {
                savedMessage(rate);
            } else {
                throw new RuntimeException("SAVING PROBLEMS IN SCHEDULED METHOD...");
            }

        }

    }

    private String savingMessage(Rate rate) {
        String message = "\n\n\n\n\nSaving rate : " + rate.getCurrencypair().getPair();
        System.out.println(message);
        return message;
    }

    private String savedMessage(Rate rate) {
        String message = "Saved rate with id : " + rate.getId();
        System.out.println(message);
        return message;
    }

}
