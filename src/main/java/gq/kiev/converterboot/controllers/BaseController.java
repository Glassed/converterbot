package gq.kiev.converterboot.controllers;

import gq.kiev.converterboot.model.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by illia on 28.08.15.
 * Changed by sasha-naturasha 01.03.16
 */
@Controller
public class BaseController {


    //@Autowired
    //MainService mainService

    @RequestMapping(value = "/", method= RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("user", new User());
        return "index";
    }

    @RequestMapping(value="/index", method= RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute User user, Model model) {
        String userName = userDetector(user);
        user.setName(userName);

        model.addAttribute("user", user);
        return "result";
    }

    private static String userDetector(User user){
        String name = user.getName();
        final String[] evgenPrefixes = {"Ев","Же","ев","же","Ev","ev","Кл","кл","Kl"
                ,"kl"};
        final String[] iliaPrefixes = {"Ил","ил","По","по","Il","il", "Po"
                ,"po"};
        final String[] alexanderPrefixes = {"Ал","ал","Са","са","Де","де","Al","al"
                ,"Sa","sa","De","de",};
        final String[][]prefixes = {evgenPrefixes,iliaPrefixes,alexanderPrefixes};

        for(int i=0; i<prefixes.length; i++){
            for(int j=0; j<prefixes[i].length; j++){
                if(name.startsWith(prefixes[i][j])){
                    switch(i){
                        case 0:
                            return "Педик";
                        case 1:
                            return "Глиномес";
                        case 2:
                            return "Господин";
                    }
                }
            }
        }

        return user.getName();

    }
}