package gq.kiev.converterboot.dao;

import gq.kiev.converterboot.model.db.Scale;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by illia on 14.03.16.
 */
public interface ScaleDao extends CrudRepository<Scale, Integer> {
}
