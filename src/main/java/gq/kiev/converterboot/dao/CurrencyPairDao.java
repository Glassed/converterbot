package gq.kiev.converterboot.dao;

import gq.kiev.converterboot.model.db.CurrencyPair;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by illia on 14.03.16.
 */
public interface CurrencyPairDao extends CrudRepository<CurrencyPair, Integer> {

}
