package gq.kiev.converterboot.dao;

import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.api.Pair;
import org.joda.time.LocalDate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by illia on 14.03.16. Changed by Alexander on 25.03.16
 */
public interface RateDao extends CrudRepository<Rate, Integer> {

    Rate findByCreatedDateAndCurrencypairIdAndScaleId(LocalDate crearedDate, Integer curId, Integer scaleId);

    @Query("select r from Rate r "
            + "where r.currencypair.id=:cpid and r.scale.id=:sid "
            + "and r.createdDate >= :from and r.createdDate <= :to "
            + "order by r.createdDate asc")
    List<Rate> getRateAccordingToPeriod(@Param("cpid") int currencypairId,
            @Param("sid") int scaleId,
            @Param("from") LocalDate from,
            @Param("to") LocalDate to);

    @Query("SELECT  AVG(rate.buy)"
            + " FROM Rate AS rate"
            + " WHERE rate.currencypair.id=:cpid"
            + " AND rate.scale.id=:sid"
            + " AND rate.createdDate >= :from"
            + " AND rate.createdDate <= :to")
    Double getAvgBuy(
            @Param("cpid") int currencypairId,
            @Param("sid") int scaleId,
            @Param("from") LocalDate from,
            @Param("to") LocalDate to);
    
    @Query("SELECT  AVG(rate.sale)"
            + " FROM Rate AS rate"
            + " WHERE rate.currencypair.id=:cpid"
            + " AND rate.scale.id=:sid"
            + " AND rate.createdDate >= :from"
            + " AND rate.createdDate <= :to")
    Double getAvgSale(
            @Param("cpid") int currencypairId,
            @Param("sid") int scaleId,
            @Param("from") LocalDate from,
            @Param("to") LocalDate to);

    @Query("SELECT NEW gq.kiev.converterboot.model.api.Pair(AVG(rate.buy), AVG(rate.sale))"
            + " FROM Rate AS rate"
            + " WHERE rate.currencypair.id=:cpid"
            + " AND rate.scale.id=:sid"
            + " AND rate.createdDate >= :from"
            + " AND rate.createdDate <= :to")
     Pair getAvgPair(
            @Param("cpid") int currencypairId,
            @Param("sid") int scaleId,
            @Param("from") LocalDate from,
            @Param("to") LocalDate to);

//    @Query("SELECT  "
//            + "NEW Rate( "
//            + "rate.id, "
//            + "rate.changedDate, "
//            + "rate.createdDate, "
//            + "max(rate.buy), "
//            + "max(rate.sale), "
//            + "rate.currencypair, "
//            + "rate.scale) "
//            + "FROM Rate rate "
//            + " WHERE rate.currencypair.id=:cpid"
//            + " AND rate.scale.id=:sid"
//            + " AND rate.createdDate >= :from "
//            + " AND rate.createdDate <= :to "
//            + "GROUP BY rate.id")
//    List<Rate> getRate(@Param("cpid") int currencypairId,
//            @Param("sid") int scaleId,
//            @Param("from") LocalDate from,
//            @Param("to") LocalDate to);
}
