/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.service.receiver.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.api.PrivateApiEntity;
import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.CurrencyPairService;
import gq.kiev.converterboot.service.receiver.JsonDataReceiver;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alexander
 */
@Service
public class JsonDataReceiverImpl implements JsonDataReceiver {
    
    private static final ScaleEnum DEFAULT_SCALE = ScaleEnum.DAY;
    
    @Value("${application.main.url}")
    private String url;
    
    @Autowired
    CurrencyPairService currencyPairService;

    /**
     * Return  Rate that corresponding to given CurrencyPairEnum object
     * May return null if nothing found
     * @param currencyPair
     * @return
     */
    @Override
    public Rate receiveRate(CurrencyPairEnum currencyPair){
        for(Rate rate : receiveRates()){
            String pairName = rate.getCurrencypair().getPair();
            if(pairName.equalsIgnoreCase(currencyPair.getName())){
                return rate;
            }
        }
        return null;
    }

    @Override
    public List<Rate> receiveRates() {
        List<Rate> result = new ArrayList<>();
        List<PrivateApiEntity> currencies = receiveCurrencies();
        for (PrivateApiEntity currency : currencies) {
            final Rate rate = new Rate();
            rate.setCreatedDate(LocalDate.now());
            rate.setBuy(currency.getBuy());
            rate.setSale(currency.getSale());

            final CurrencyPair pair =
                    currencyPairService.getCurrencyPair(currency.getPair());
            if (pair == null) {
                continue;
            }
            rate.setCurrencypair(pair);
            rate.setScale(getScale());

            result.add(rate);
        }

        return result;
    }

    private Scale getScale() {
        Scale result = new Scale();
        result.setScale(DEFAULT_SCALE.getName());
        result.setId(DEFAULT_SCALE.getId());
        return result;
    }


    private List<PrivateApiEntity> receiveCurrencies() {

        //creates type ArrayList<Currency>.class
        Type listOfTypes = new TypeToken<ArrayList<PrivateApiEntity>>() {
        }.getType();
        return new Gson().fromJson(getJsonData(), listOfTypes);
    }

    private String getJsonData() {
        StringBuilder data = new StringBuilder();
        try {
            URL link = new URL(url);
            Reader reader = new InputStreamReader(new BufferedInputStream(link.openStream()), "UTF-8");

            int ch = 0;
            while ((ch = reader.read()) != -1) {
                data.append((char) ch);
            }
            return data.toString();

        } catch (MalformedURLException ex) {
            Logger.getLogger(JsonDataReceiverImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JsonDataReceiverImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

}
