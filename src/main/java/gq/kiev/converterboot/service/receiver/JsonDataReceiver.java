/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.service.receiver;

import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import java.util.List;

/**
 *
 * @author Alexander
 */
public interface JsonDataReceiver {
    
    Rate receiveRate(CurrencyPairEnum currencyPair);
    List<Rate> receiveRates();
    
    
}
