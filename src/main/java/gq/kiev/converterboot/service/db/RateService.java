package gq.kiev.converterboot.service.db;

import gq.kiev.converterboot.model.db.Rate;
import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by illia on 26.03.16.
 */
public interface RateService {

    List<Rate> getRatesByScalePairAndDates(String scaleName, String pairName,
                                           LocalDate from, LocalDate to);
    
    //TODO: write implementation!
    Rate getRateWithAverBuyAndSale(String scaleName, String pairName,
                                           LocalDate from, LocalDate to);
}
