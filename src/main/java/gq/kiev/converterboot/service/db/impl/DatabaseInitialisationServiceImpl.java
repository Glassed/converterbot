package gq.kiev.converterboot.service.db.impl;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.CurrencyPairEnum;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


@Service
public class DatabaseInitialisationServiceImpl implements DatabaseInitialisationService {
    private static final Logger LOG = Logger.getLogger(DatabaseInitialisationServiceImpl.class.getName());
    @Autowired
    private ScaleDao scaleDao;
    
    @Autowired
    private CurrencyPairDao pairDao;

    @Override
    public void initScale() {
        final Set<Scale> availableScaleSet = new HashSet<>();
        for (Scale scale : scaleDao.findAll()) {
            availableScaleSet.add(scale);
        }

        final List<Scale> predefScales = new ArrayList<>(ScaleEnum.values().length);
        for (ScaleEnum scaleEnum : ScaleEnum.values()) {

            final Scale newScale = new Scale(scaleEnum.getId(), scaleEnum.getName());
            if (!availableScaleSet.contains(newScale)) {
                LOG.log(Level.INFO, "adding scale into DB : " + newScale);
                predefScales.add(newScale);
            }
        }
        if(!predefScales.isEmpty())
        scaleDao.save(predefScales);
    }

    @Autowired
    private CurrencyPairDao currencyPairDao;

    @Override
    public void initCurrencyPair() {
        if (currencyPairDao.count() > 0) {
            return;
        }
        
        final Set<CurrencyPair> avaliablePairSet = new HashSet<>();
        for(CurrencyPair pair : pairDao.findAll()){
            avaliablePairSet.add(pair);
        }
        
        final List<CurrencyPair> predefCurrencyPairs = new ArrayList<>(CurrencyPairEnum.values().length);
        for (CurrencyPairEnum currencyPairEnum : CurrencyPairEnum.values()) {
            
            CurrencyPair newCurrencyPair = new CurrencyPair(currencyPairEnum.getId()
                    , currencyPairEnum.getName());
            if(!avaliablePairSet.contains(newCurrencyPair)){
                LOG.log(Level.INFO, "adding currencyPair into DB : " + newCurrencyPair);
                predefCurrencyPairs.add(newCurrencyPair);
            }            
        }
        if(!predefCurrencyPairs.isEmpty())
        currencyPairDao.save(predefCurrencyPairs);
    }
}
