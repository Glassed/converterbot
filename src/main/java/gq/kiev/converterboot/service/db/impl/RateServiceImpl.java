package gq.kiev.converterboot.service.db.impl;

import gq.kiev.converterboot.dao.RateDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.model.db.Rate;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.model.db.stable.ScaleEnum;
import gq.kiev.converterboot.service.db.CurrencyPairService;
import gq.kiev.converterboot.service.db.RateService;
import gq.kiev.converterboot.service.db.ScaleService;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by illia on 26.03.16.
 */
@Service
public class RateServiceImpl implements RateService {
    private static final Logger logger = LoggerFactory.getLogger(RateService.class);

    @Autowired
    private RateDao rateDao;

    @Autowired
    private CurrencyPairService currencyPairService;

    @Autowired
    private ScaleService scaleService;

    @Override
    public List<Rate> getRatesByScalePairAndDates(String scaleName, String pairName, LocalDate from, LocalDate to) {
        validateNotNull(scaleName, "scale cannot be null");
        validateNotNull(pairName, "currency pair cannot be null");
        validateNotNull(from, "date from cannot be null");
        validateNotNull(to, "date to cannot be null");

        final LocalDate normalizedFrom = normalizeDate(from, scaleName);
        final LocalDate normalizedTo = normalizeDate(to, scaleName);
        final Scale retrievedScale = scaleService.getByName(scaleName);
        final CurrencyPair pair = currencyPairService.getCurrencyPair(pairName);

        validateNotNull(retrievedScale, "scale : " + scaleName + " have not been created yet");
        validateNotNull(pair, "currency pair : " + pairName + " have not been created yet");

        final List<Rate> rates = rateDao.getRateAccordingToPeriod(
                pair.getId(), retrievedScale.getId(),
                normalizedFrom, normalizedTo);

        // todo remove this in future
        logger.info(rates.toString());

        return rates;
    }


    private void validateNotNull(final Object obj, final String message) {
        if (obj == null) {
            throw new IllegalArgumentException(message);
        }
    }

    private LocalDate normalizeDate(final LocalDate date, final String scaleName) {
        final ScaleEnum scaleStable = ScaleEnum.valueOf(scaleName);

        switch (scaleStable) {
            case MONTH: return date.withDayOfMonth(1);
            case YEAR: return date.withDayOfYear(1);
            default: return date;
        }
    }

    @Override
    public Rate getRateWithAverBuyAndSale(String scaleName, String pairName, LocalDate from, LocalDate to) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
