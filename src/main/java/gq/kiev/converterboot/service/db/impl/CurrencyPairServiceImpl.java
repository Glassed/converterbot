package gq.kiev.converterboot.service.db.impl;

import gq.kiev.converterboot.dao.CurrencyPairDao;
import gq.kiev.converterboot.model.db.CurrencyPair;
import gq.kiev.converterboot.service.db.CurrencyPairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by illia on 22.03.16.
 */
@Service
public class CurrencyPairServiceImpl implements CurrencyPairService {
    private final Map<String, CurrencyPair> currencyByName;

    private CurrencyPairDao dao;

    @Autowired
    public CurrencyPairServiceImpl(CurrencyPairDao dao) {
        this.dao = dao;
        currencyByName = new ConcurrentHashMap<>();
        getHachedMap();
    }

    private void getHachedMap() {
        for (CurrencyPair currencyPair : dao.findAll()) {
            currencyByName.put(currencyPair.getPair(), currencyPair);
        }
    }

    @Override
    public CurrencyPair getCurrencyPair(final String name) {
        if (!currencyByName.containsKey(name)) {
            getHachedMap();
        }
        return currencyByName.get(name);
    }
}
