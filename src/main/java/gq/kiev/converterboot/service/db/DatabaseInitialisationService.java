package gq.kiev.converterboot.service.db;

/**
 * Created by illia on 15.03.16.
 *
 * Save needed initial data to proper work with Rate entities in db
 * Methods of class should be called in order to start working with application's
 * functional
 */

public interface DatabaseInitialisationService {
    
    /**
     * Initialize needed Scale entities in db if they're not yet exist.
     */
    void initScale();
    
    /**
     * Initialize needed CurrencyPair entities in db if they're not yet exist.
     */
    void initCurrencyPair();
}
