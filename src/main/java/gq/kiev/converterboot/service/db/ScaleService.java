package gq.kiev.converterboot.service.db;

import gq.kiev.converterboot.model.db.Scale;

/**
 * Created by illia on 25.03.16.
 */
public interface ScaleService {

    Scale getByName(String scaleName);

}
