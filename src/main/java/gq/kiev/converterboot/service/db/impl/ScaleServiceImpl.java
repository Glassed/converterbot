package gq.kiev.converterboot.service.db.impl;

import gq.kiev.converterboot.dao.ScaleDao;
import gq.kiev.converterboot.model.db.Scale;
import gq.kiev.converterboot.service.db.ScaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ScaleServiceImpl implements ScaleService {
    private final Map<String, Scale> scaleByName;

    private ScaleDao dao;

    @Autowired
    public ScaleServiceImpl(ScaleDao dao) {
        this.dao = dao;
        scaleByName = new ConcurrentHashMap<>();
        getHachedMap();
    }

    private void getHachedMap() {
        for (Scale scale : dao.findAll()) {
            scaleByName.put(scale.getScale(), scale);
        }
    }

    @Override
    public Scale getByName(String scaleName) {
        if(!scaleByName.containsKey(scaleName)) {
            getHachedMap();
        }
        return scaleByName.get(scaleName);
    }
}
