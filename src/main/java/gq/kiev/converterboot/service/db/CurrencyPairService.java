package gq.kiev.converterboot.service.db;

import gq.kiev.converterboot.model.db.CurrencyPair;

/**
 * Created by illia on 22.03.16.
 */
public interface CurrencyPairService {
    //ASK ask Ilia how it would be better to add new CurrencyPair in runtime
    /**
     * Returns CurrencyPair object or null if such currency pair cannot be stored
     * (New pairs can be added if .. 
     * 
     * @param name
     * @return 
     */
    CurrencyPair getCurrencyPair(String name);
}
