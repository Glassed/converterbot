package gq.kiev.converterboot.service.convert;

import java.util.ArrayList;
import java.util.List;
import gq.kiev.converterboot.model.googleapi.ColumnEntity;
import gq.kiev.converterboot.model.googleapi.RowEntity;

/**
 *
 * @author Alexander
 */
public class JsonGoogleAdapterController {

    private List<ColumnEntity> cols;
    private List<RowEntity> rows;

    public JsonGoogleAdapterController() {
        cols = new ArrayList<>();
        rows = new ArrayList<>();
    }

    /**
     *
     * @param scale that parameter can receive "whole","year","month","day"
     * values; if does not entered any of that value would be "whole";
     * @param dataList
     * @param ccyIndex
     * @return
     */  
    
    
//    public static JsonGoogleAdapterController getJsonData(String scale, List<Maintable> dataList, int ccyIndex) {
//        JsonGoogleAdapterController result = new JsonGoogleAdapterController();
//
//        //marking first column of data table:
//        String mainColumnMark = getMainColumnMark(scale);
//        List<ColumnEntity> cols = getColumns(mainColumnMark);
//
//        List<RowEntity> rows = getRows(scale, dataList, ccyIndex);
//
//        result.setCols(cols);
//        result.setRows(rows);
//
//        return result;
//    }
//    
//    public static String getJsonStringData(String scale, List<Maintable> dataList, int ccyIndex) {
//        Gson gson = new GsonBuilder().create();        
//        return gson.toJson(getJsonData(scale, dataList, ccyIndex));
//    }
//    
//
//    private static List<RowEntity> getRows(String scale, List<Maintable> dataList, int ccyIndex) {
//        List<RowEntity> result = new ArrayList<>();
//
//        for (Maintable maintable : dataList) {
//            Currency currency = maintable.getCurrency(ccyIndex);
//            String v1 = getFirstOperationValue(scale, maintable);
//            result.add(createRowEntity(v1, currency));
//        }
//        return result;
//    }
//
//    private static RowEntity createRowEntity(String v1, Currency currency) {
//        String buy = currency.getBuy();
//        String sale = currency.getSale();
//        RowEntity result = new RowEntity();
//        List<RowSocketEntity> c = new ArrayList<>();
//        c.add(new RowSocketEntity(v1));
//        c.add(new RowSocketEntity(buy));
//        c.add(new RowSocketEntity(sale));
//        result.setC(c);
//        return result;
//    }
//
//    private static List<ColumnEntity> getColumns(String tableHvalueName) {
//        List<ColumnEntity> cols = new ArrayList<>();
//        cols.add(new ColumnEntity(null, tableHvalueName, null, "number"));
//        cols.add(new ColumnEntity(null, "Buy", null, "number"));
//        cols.add(new ColumnEntity(null, "Sale", null, "number"));
//
//        return cols;
//    }
//
//    public List<ColumnEntity> getCols() {
//        return cols;
//    }
//
//    public void setCols(List<ColumnEntity> cols) {
//        this.cols = cols;
//    }
//
//    public List<RowEntity> getRows() {
//        return rows;
//    }
//
//    public void setRows(List<RowEntity> rows) {
//        this.rows = rows;
//    }
//
//    private static String getMainColumnMark(String scale) {
//        switch (scale) {
//            case "whole":
//                return "Year";
//            case "year":
//                return "Month";
//            case "month":
//                return "Day";
//            case "day":
//                return "Time";
//            default:
//                return "Year";
//        }
//    }
//
//    private static String getFirstOperationValue(String scale, Maintable maintable) {
//        switch (scale) {
//            case "whole":
//                return maintable.getYear();
//            case "year":
//                return maintable.getMonth();
//            case "month":
//                return maintable.getDay();
//            case "day":
//                return maintable.getTime();
//            default:
//                return maintable.getYear();
//        }
//    }

}
