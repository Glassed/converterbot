package gq.kiev.converterboot.model.db.stable;

/**
 * This enum contains scale names used to provide logics in app.
 */
public enum ScaleEnum {
    YEAR(1), MONTH(2), DAY(3), WHORE(4);

    private final int predefinedId;

    ScaleEnum(int predefinedId) {
        this.predefinedId = predefinedId;
    }

    public int getId() {
        return predefinedId;
    }

    public String getName() {
        return name();
    }
}
