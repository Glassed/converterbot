/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.db;

import org.joda.time.LocalDate;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Alexander
 */

@Entity
@Table(name = "rate")
public class Rate implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false)
    private double buy;

    @Column(nullable = false)
    private double sale;

    @Column(nullable = false)
    private LocalDate createdDate;

    @Column(nullable = false)
    private LocalDate changedDate;

    @JoinColumn(name = "currencypairid", referencedColumnName = "id")
    @ManyToOne
    private CurrencyPair currencypair;

    @JoinColumn(name = "scaleid", referencedColumnName = "id")
    @ManyToOne
    private Scale scale;

    public Rate() {
    }

    public Rate(LocalDate createdDate, double buy, double sale, CurrencyPair currencypair, Scale scale) {
        this.createdDate = createdDate;
        this.changedDate = createdDate;
        this.buy = buy;
        this.sale = sale;
        this.currencypair = currencypair;
        this.scale = scale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        this.changedDate = createdDate;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSale() {
        return sale;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    public CurrencyPair getCurrencypair() {
        return currencypair;
    }

    public void setCurrencypair(CurrencyPair currencypair) {
        this.currencypair = currencypair;
    }

    public Scale getScale() {
        return scale;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }

    public LocalDate getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDate changedDate) {
        this.changedDate = changedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rate rate = (Rate) o;

        if (Double.compare(rate.buy, buy) != 0) return false;
        if (Double.compare(rate.sale, sale) != 0) return false;
        if (id != null ? !id.equals(rate.id) : rate.id != null) return false;
        if (createdDate != null ? !createdDate.equals(rate.createdDate) : rate.createdDate != null) return false;
        if (changedDate != null ? !changedDate.equals(rate.changedDate) : rate.changedDate != null) return false;
        if (currencypair != null ? !currencypair.equals(rate.currencypair) : rate.currencypair != null) return false;
        return scale != null ? scale.equals(rate.scale) : rate.scale == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        temp = Double.doubleToLongBits(buy);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sale);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (changedDate != null ? changedDate.hashCode() : 0);
        result = 31 * result + (currencypair != null ? currencypair.hashCode() : 0);
        result = 31 * result + (scale != null ? scale.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "id=" + id +
                ", buy=" + buy +
                ", sale=" + sale +
                ", createdDate=" + createdDate +
                ", changedDate=" + changedDate +
                ", currencypair=" + currencypair.getId() +
                ", scale=" + scale.getId() +
                '}';
    }
}
