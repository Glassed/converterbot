/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.db;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "scale")
public class Scale implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    //ATTENTION
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String scale;
    /*//ATTENTION was cascade = CascadeType.ALL,
    @OneToMany(mappedBy = "scaleid", cascade = CascadeType.ALL)
    private Collection<Rate> rateCollection;*/

    public Scale() {
    }

    public Scale(Integer id) {
        this.id = id;
    }

    public Scale(Integer id, String scale) {
        this.id = id;
        this.scale = scale;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }
/*

    public Collection<Rate> getRateCollection() {
        return rateCollection;
    }

    public void setRateCollection(Collection<Rate> rateCollection) {
        this.rateCollection = rateCollection;
    }
*/

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.scale);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Scale other = (Scale) obj;
        if (!Objects.equals(this.scale, other.scale)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return "Scale{ id= " + id + ", scale= "+scale+" }";
    }
    
}
