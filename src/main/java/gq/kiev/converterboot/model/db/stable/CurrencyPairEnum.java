/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.db.stable;

/**
 * This enum contains all currency pair names used to store and retrieve
 * rate data from db.
 */
public enum CurrencyPairEnum {

    EURUAH(1), USDUAH(2), RURUAH(3);
    private final int predefinedId;
    
    CurrencyPairEnum(int predefinedId) {
        this.predefinedId = predefinedId;
    }

    public Integer getId() {
        return this.predefinedId;
    }

    public String getName() {
        return name();
    }
}
