/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.db;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "currencypair")
public class CurrencyPair implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    //ATTENTION
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(unique = true, nullable = false)
    private String pair;
 /*   //ATTENTION was cascade = CascadeType.ALL,
    @OneToMany(mappedBy = "currencypairid", cascade = CascadeType.ALL)
    private Collection<Rate> rateCollection;
*/
    public CurrencyPair() {
    }

    public CurrencyPair(Integer id) {
        this.id = id;
    }
    
    public CurrencyPair(String pair) {
        this.pair = pair;
    }
    
    public CurrencyPair(Integer id, String pair) {
        this.id = id;
        this.pair = pair;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

 /*   public Collection<Rate> getRateCollection() {
        return rateCollection;
    }*/

    /*public void setRateCollection(Collection<Rate> rateCollection) {
        this.rateCollection = rateCollection;
    }*/

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.pair);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CurrencyPair other = (CurrencyPair) obj;
        if (!Objects.equals(this.pair, other.pair)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return "CurrencyPair{ id= " + id + ", pair= "+pair+"}";
    }
    
}
