/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.googleapi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alexander
 */
public class RowEntity {
    private List<RowSocketEntity> c = new ArrayList<>();

    public List<RowSocketEntity> getC() {
        return c;
    }

    public void setC(List<RowSocketEntity> c) {
        this.c = c;
    }

    public RowEntity(List<RowSocketEntity> c) {
        this.c = c;
    }

    public RowEntity() {
    }
    
    
}
