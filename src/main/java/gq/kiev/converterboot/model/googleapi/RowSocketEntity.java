/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.googleapi;

/**
 *
 * @author Alexander
 */
public class RowSocketEntity {
    private String v = "";

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public RowSocketEntity() {
    }
    
    public RowSocketEntity(String v) {
        this.v = v;
    }
    
}
