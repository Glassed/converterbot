/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gq.kiev.converterboot.model.api;

import java.util.Objects;

public class PrivateApiEntity {
    String ccy;
    String base_ccy;
    double buy;
    double sale; 

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getBase_ccy() {
        return base_ccy;
    }

    public void setBase_ccy(String base_ccy) {
        this.base_ccy = base_ccy;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSale() {
        return sale;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    public String getPair() {
        return getCcy() + getBase_ccy();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.ccy);
        hash = 29 * hash + Objects.hashCode(this.base_ccy);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.buy) ^ (Double.doubleToLongBits(this.buy) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.sale) ^ (Double.doubleToLongBits(this.sale) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PrivateApiEntity other = (PrivateApiEntity) obj;
        if (!Objects.equals(this.ccy, other.ccy)) {
            return false;
        }
        if (!Objects.equals(this.base_ccy, other.base_ccy)) {
            return false;
        }
        if (Double.doubleToLongBits(this.buy) != Double.doubleToLongBits(other.buy)) {
            return false;
        }
        if (Double.doubleToLongBits(this.sale) != Double.doubleToLongBits(other.sale)) {
            return false;
        }
        return true;
    }
    
    
    
}
