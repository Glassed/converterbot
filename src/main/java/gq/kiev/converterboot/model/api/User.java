package gq.kiev.converterboot.model.api;

/**
 * Created by illia on 28.02.16.
 */
public class User {

    private Long id;
    private String name;
    private Boolean natural;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getNatural() {
        return natural;
    }

    public void setNatural(Boolean natural) {
        this.natural = natural;
    }
}
