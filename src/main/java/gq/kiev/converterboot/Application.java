package gq.kiev.converterboot;

import gq.kiev.converterboot.service.db.DatabaseInitialisationService;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import org.springframework.scheduling.annotation.EnableScheduling;

//enable scheduled tasks executing:
@EnableScheduling

@EnableAutoConfiguration
@ComponentScan("gq.kiev.converterboot")
@Configuration
@EntityScan(basePackages = "gq.kiev.converterboot.model.db")
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
        registration.addUrlMappings("/console/*");
        return registration;
    }

    @Autowired
    DatabaseInitialisationService initService;

    @PostConstruct
    public void fillDatabase() {
        initService.initScale();
        initService.initCurrencyPair();
    }

}
